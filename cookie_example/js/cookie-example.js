(function ($, Drupal) {
    $('.cookie-item a').on('click', function(event) {
        event.preventDefault();

        var $link = $(this);
        var id = $link.parent().data('id');

        if ($link.attr('class') == 'del') {
            $.post(
                '/cookie-example/delete/' + id,
                { id: id },
                function() {
                    $link.removeClass('del').addClass('add').html('Add');
                }
            )
        } else {
            $.post(
                '/cookie-example/set/' + id,
                { id: id },
                function() {
                    $link.removeClass('add').addClass('del').html('Remove');
                }
            )
        }
    });
})(jQuery, Drupal);