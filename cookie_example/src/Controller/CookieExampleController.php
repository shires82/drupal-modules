<?php

namespace Drupal\cookie_example\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\PageCache\ResponsePolicy\KillSwitch;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Cookie;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class CookieExampleController
 *
 * @package Drupal\cookie_example\Controller
 */
class CookieExampleController extends ControllerBase {

  /**
   * @var null|Request
   */
  protected $request;

  /**
   * @var KillSwitch
   */
  protected $cacheKillSwitch;

  /**
   * CookieExampleController constructor.
   *
   * @param RequestStack $requestStack
   */
  public function __construct(RequestStack $requestStack) {
    $this->request = $requestStack->getCurrentRequest();
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('request_stack')
    );
  }

  /**
   * Gets the example cookie and displays on the page.
   *
   * @return array
   */
  public function getCookie() {
    $cookie = (array) json_decode($this->request->cookies->get('example_cookie'));

    return [
      '#theme' => 'cookie_example',
      '#cookie' => $cookie,
      '#attached' =>  ['library' => ['cookie_example/cookie-example']],
    ];
  }

  /**
   * Sets a cookie and redirects.
   *
   * @param int $id
   *
   * @return Response
   */
  public function setCookie($id) {
    $cookie = (array) json_decode($this->request->cookies->get('example_cookie'));
    $cookie[$id] = REQUEST_TIME;

    $response = new Response();
    $response->headers->setCookie(new Cookie('example_cookie', json_encode($cookie), 0, '/' , NULL, FALSE));

    return $response;
  }

  /**
   * Deletes from a cookie and redirects.
   *
   * @param int $id
   *
   * @return Response
   */
  public function deleteCookie($id) {
    $cookie = json_decode($this->request->cookies->get('example_cookie'));

    if (isset($cookie->{$id})) {
      unset($cookie->{$id});
    }

    $response = new Response();
    $response->headers->setCookie(new Cookie('example_cookie', json_encode($cookie), 0, '/' , NULL, FALSE));

    return $response;
  }

  /**
   * Removes the cookie.
   *
   * @return RedirectResponse
   */
  public function removeCookie() {
    $response = new RedirectResponse('/cookie-example');
    $response->headers->clearCookie('example_cookie');

    return $response;
  }
}
